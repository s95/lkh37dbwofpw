<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'drupal/legacy-project',
  ),
  'versions' => 
  array (
    'asm89/stack-cors' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9c31def6a83f84b4d4a40d35996d375755f0e08',
    ),
    'chi-teck/drupal-code-generator' => 
    array (
      'pretty_version' => '1.33.1',
      'version' => '1.33.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f814e980b6f9cf1ca8c74cc9385c3d81090d388',
    ),
    'composer/installers' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1a0357fccad9d1cc1ea0c9a05b8847fbccccb78d',
    ),
    'composer/package-versions-deprecated' => 
    array (
      'pretty_version' => '1.11.99.1',
      'version' => '1.11.99.1',
      'aliases' => 
      array (
      ),
      'reference' => '7413f0b55a051e89485c5cb9f765fe24bb02a7b6',
    ),
    'composer/semver' => 
    array (
      'pretty_version' => '3.2.2',
      'version' => '3.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4089fddb67bcf6bf860d91b979e95be303835002',
    ),
    'consolidation/annotated-command' => 
    array (
      'pretty_version' => '4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ec297e05cb86557671c2d6cbb1bebba6c7ae2c60',
    ),
    'consolidation/config' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cac1279bae7efb5c7fb2ca4c3ba4b8eb741a96c1',
    ),
    'consolidation/filter-via-dot-access-data' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a53e96c6b9f7f042f5e085bf911f3493cea823c6',
    ),
    'consolidation/log' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '82a2aaaa621a7b976e50a745a8d249d5085ee2b1',
    ),
    'consolidation/output-formatters' => 
    array (
      'pretty_version' => '4.1.2',
      'version' => '4.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5821e6ae076bf690058a4de6c94dce97398a69c9',
    ),
    'consolidation/robo' => 
    array (
      'pretty_version' => '2.2.2',
      'version' => '2.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b365df174d9cfb0f5814e4f3275a1c558b17bc4c',
    ),
    'consolidation/self-update' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dba6b2c0708f20fa3ba8008a2353b637578849b4',
    ),
    'consolidation/site-alias' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9ed3c590be9fcf9fea69c73456c2fd4b27f5204c',
    ),
    'consolidation/site-process' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ef57711d7049f7606ce936ded16ad93f1ad7f02c',
    ),
    'container-interop/container-interop' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '79cbf1341c22ec75643d841642dd5d6acd83bdb8',
    ),
    'container-interop/container-interop-implementation' => 
    array (
      'provided' => 
      array (
        0 => '^1.2',
      ),
    ),
    'dflydev/dot-access-data' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3fbd874921ab2c041e899d044585a2ab9795df8a',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ce77a7ba1770462cd705a91a151b6c3746f9c6ad',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a9c1b59eba5a08ca2770a76eddb88922f504e8e0',
    ),
    'doctrine/collections' => 
    array (
      'pretty_version' => '1.6.7',
      'version' => '1.6.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '55f8b799269a1a472457bd1a41b4f379d4cfba4a',
    ),
    'doctrine/common' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a036d90c303f3163b5be8b8fde9b6755b2be4a3a',
    ),
    'doctrine/data-fixtures' => 
    array (
      'pretty_version' => '1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '51d3d4880d28951fff42a635a2389f8c63baddc5',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => '2.13.1',
      'version' => '2.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c800380457948e65bbd30ba92cc17cda108bf8c9',
    ),
    'doctrine/deprecations' => 
    array (
      'pretty_version' => 'v0.5.3',
      'version' => '0.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9504165960a1f83cc1480e2be1dd0a0478561314',
    ),
    'doctrine/doctrine-bundle' => 
    array (
      'pretty_version' => '2.3.1',
      'version' => '2.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a08bc3b4d8567cdff05e89b272ba1e06e9d71c21',
    ),
    'doctrine/doctrine-fixtures-bundle' => 
    array (
      'pretty_version' => '3.4.0',
      'version' => '3.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '870189619a7770f468ffb0b80925302e065a3b34',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9cf661f4eb38f7c881cac67c75ea9b00bf97b210',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'doctrine/orm' => 
    array (
      'pretty_version' => '2.8.4',
      'version' => '2.8.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a588555ecd837b8d7e89355d9a13902e54d529c7',
    ),
    'doctrine/persistence' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9899c16934053880876b920a3b8b02ed2337ac1d',
    ),
    'doctrine/reflection' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa587178be682efe90d005e3a322590d6ebb59a5',
    ),
    'doctrine/sql-formatter' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '56070bebac6e77230ed7d306ad13528e60732871',
    ),
    'drupal/action' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/aggregator' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/automated_cron' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/ban' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/bartik' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/basic_auth' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/big_pipe' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/block' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/block_content' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/book' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/breakpoint' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/ckeditor' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/claro' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/classy' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/color' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/comment' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/config' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/config_translation' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/contact' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/content_moderation' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/content_translation' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/contextual' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core' => 
    array (
      'pretty_version' => '9.1.7',
      'version' => '9.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '0cbed9e7b0243ccc89a3735853eedb40becf8095',
    ),
    'drupal/core-annotation' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-assertion' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-bridge' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-class-finder' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-composer-scaffold' => 
    array (
      'pretty_version' => '9.1.7',
      'version' => '9.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '7b125516d6568b888945ee03ac2636dcced76e8d',
    ),
    'drupal/core-datetime' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-dependency-injection' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-diff' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-discovery' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-event-dispatcher' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-file-cache' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-file-security' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-filesystem' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-front-matter' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-gettext' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-graph' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-http-foundation' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-php-storage' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-plugin' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-project-message' => 
    array (
      'pretty_version' => '9.1.7',
      'version' => '9.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '812d6da43dd49cc210af62e80fa92189e68e565a',
    ),
    'drupal/core-proxy-builder' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-recommended' => 
    array (
      'pretty_version' => '9.1.7',
      'version' => '9.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ced38da112867c8fff15d5079926c95e5bcdca5b',
    ),
    'drupal/core-render' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-serialization' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-transliteration' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-utility' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-uuid' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/core-vendor-hardening' => 
    array (
      'pretty_version' => '9.1.7',
      'version' => '9.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '71df53f24d54c464ac18762a530fc7c3bf131a7f',
    ),
    'drupal/core-version' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/country_state_city' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8.x-2.1',
    ),
    'drupal/datetime' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/datetime_range' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/dblog' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/dynamic_page_cache' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/editor' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/entity' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8.x-1.2',
    ),
    'drupal/entity_reference' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/field' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/field_layout' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/field_ui' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/file' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/filter' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/fivestar' => 
    array (
      'pretty_version' => 'dev-1.x',
      'version' => 'dev-1.x',
      'aliases' => 
      array (
        0 => '1.x-dev',
      ),
      'reference' => '067bb53bcbec6d750437376d5f51f12e7a878ed4',
    ),
    'drupal/forum' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/hal' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/help' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/help_topics' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/history' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/image' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/inline_form_errors' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/jsonapi' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/language' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/layout_builder' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/layout_discovery' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/legacy-project' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'drupal/link' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/locale' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/mailsystem' => 
    array (
      'pretty_version' => '4.3.0',
      'version' => '4.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8.x-4.3',
    ),
    'drupal/media' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/media_library' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/menu_link_content' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/menu_ui' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/migrate' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/migrate_drupal' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/migrate_drupal_multilingual' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/migrate_drupal_ui' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/minimal' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/multiple_registration' => 
    array (
      'pretty_version' => '2.99.0',
      'version' => '2.99.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8.x-2.99',
    ),
    'drupal/node' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/olivero' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/options' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/page_cache' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/path' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/path_alias' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/profile' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8.x-1.2',
    ),
    'drupal/quickedit' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/rdf' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/responsive_image' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/rest' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/search' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/sendgrid_integration' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8.x-1.7',
    ),
    'drupal/serialization' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/settings_tray' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/seven' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/shortcut' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/smtp' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8.x-1.0',
    ),
    'drupal/standard' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/stark' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/statistics' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/syslog' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/system' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/taxonomy' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/telephone' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/text' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/toolbar' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/tour' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/tracker' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/update' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/user' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/views' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/views_bootstrap' => 
    array (
      'pretty_version' => '4.3.0',
      'version' => '4.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8.x-4.3',
    ),
    'drupal/views_ui' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/votingapi' => 
    array (
      'pretty_version' => 'dev-3.x',
      'version' => 'dev-3.x',
      'aliases' => 
      array (
        0 => '3.x-dev',
      ),
      'reference' => 'f88cc973c3aaaf8738653bceeafee901cf7d0c77',
    ),
    'drupal/workflows' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drupal/workspaces' => 
    array (
      'replaced' => 
      array (
        0 => '9.1.7',
      ),
    ),
    'drush/drush' => 
    array (
      'pretty_version' => '10.4.3',
      'version' => '10.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '472ce3e0ac48b418f8168a053b3bcb4c8582bf99',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '2.1.22',
      'version' => '2.1.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '68e418ec08fbfc6f58f6fd2eea70ca8efc8cc7d5',
    ),
    'enlightn/security-checker' => 
    array (
      'pretty_version' => 'v1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2054fbce2f8df681c8f71b36656222bcd241b77f',
    ),
    'fastglass/sendgrid' => 
    array (
      'pretty_version' => '1.0.12',
      'version' => '1.0.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '2f118ad67787899a84a95a870fb90a67949c3b82',
    ),
    'grasmash/expander' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '95d6037344a4be1dd5f8e0b0b2571a28c397578f',
    ),
    'grasmash/yaml-expander' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3f0f6001ae707a24f4d9733958d77d92bf9693b1',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.5.5',
      'version' => '6.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d4290de1cfd701f38099ef7e183b64b4b7b0c5e',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '60d379c243457e073cff02bc323a2a86cb355631',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '53330f47520498c0ae1f61f7e2c90f55690c06a3',
    ),
    'html2text/html2text' => 
    array (
      'pretty_version' => '4.3.1',
      'version' => '4.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '61ad68e934066a6f8df29a3d23a6460536d0855c',
    ),
    'jetbrains/phpstorm-stubs' => 
    array (
      'pretty_version' => 'v2019.3',
      'version' => '2019.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '883b6facd78e01c0743b554af86fa590c2573f40',
    ),
    'laminas/laminas-diactoros' => 
    array (
      'pretty_version' => '2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ff7400c1c12e404144992ef43c8b733fd9ad516',
    ),
    'laminas/laminas-escaper' => 
    array (
      'pretty_version' => '2.7.0',
      'version' => '2.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e04bc5ae5990b17159d79d331055e2c645e5cc5',
    ),
    'laminas/laminas-feed' => 
    array (
      'pretty_version' => '2.13.0',
      'version' => '2.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fb89aac1984222227f37792dd193d34829a0762f',
    ),
    'laminas/laminas-stdlib' => 
    array (
      'pretty_version' => '3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9d84eaa39fde733356ea948cdef36c631f202b6',
    ),
    'laminas/laminas-zendframework-bridge' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6ede70583e101030bcace4dcddd648f760ddf642',
    ),
    'league/container' => 
    array (
      'pretty_version' => '2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8438dc47a0674e3378bcce893a0a04d79a2c22b3',
    ),
    'mailjet/mailjet-apiv3-php' => 
    array (
      'pretty_version' => 'v1.5.1',
      'version' => '1.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7b94fa629d46fa5ba3826ed4596674942944520d',
    ),
    'masterminds/html5' => 
    array (
      'pretty_version' => '2.7.4',
      'version' => '2.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '9227822783c75406cfe400984b2f095cdf03d417',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.10.4',
      'version' => '4.10.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6d052fc58cb876152f89f532b95a8d7907e7f0e',
    ),
    'ocramius/package-versions' => 
    array (
      'replaced' => 
      array (
        0 => '1.11.99',
      ),
    ),
    'orno/di' => 
    array (
      'replaced' => 
      array (
        0 => '~2.0',
      ),
    ),
    'pear/archive_tar' => 
    array (
      'pretty_version' => '1.4.13',
      'version' => '1.4.13.0',
      'aliases' => 
      array (
      ),
      'reference' => '2b87b41178cc6d4ad3cba678a46a1cae49786011',
    ),
    'pear/console_getopt' => 
    array (
      'pretty_version' => 'v1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a41f8d3e668987609178c7c4a9fe48fecac53fa0',
    ),
    'pear/pear-core-minimal' => 
    array (
      'pretty_version' => 'v1.10.10',
      'version' => '1.10.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '625a3c429d9b2c1546438679074cac1b089116a7',
    ),
    'pear/pear_exception' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbb42a5a0e45f3adcf99babfb2a1ba77b8ac36a7',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.2.2',
      'version' => '5.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '069a785b2141f5bcf49f3e353548dc1cce6df556',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6a467b8989322d92aa1c8bf2bebcc6e5c2ba55c0',
    ),
    'phpmailer/phpmailer' => 
    array (
      'pretty_version' => 'v6.4.0',
      'version' => '6.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '050d430203105c27c30efd1dce7aa421ad882d01',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '^1.0',
        1 => '1.0',
      ),
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psy/psysh' => 
    array (
      'pretty_version' => 'v0.10.8',
      'version' => '0.10.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e4573f47750dd6c92dca5aee543fa77513cbd8d3',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'roave/better-reflection' => 
    array (
      'pretty_version' => '4.12.2',
      'version' => '4.12.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '73c376c7245b2928837ed1e8bef446f57f1148a0',
    ),
    'roave/signature' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b5bb9499cfbcc78d9f472e03af1e97e4341ec7c',
    ),
    'roundcube/plugin-installer' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'rsky/pear-core-min' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.10.10',
      ),
    ),
    'sendgrid/sendgrid-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'sendgrid/smtpapi' => 
    array (
      'pretty_version' => '0.6.7',
      'version' => '0.6.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cf63bd353c25f775a1fb2654a0a3fff22f573a37',
    ),
    'sendgrid/smtpapi-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'shama/baton' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'stack/builder' => 
    array (
      'pretty_version' => 'v1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a4faaa6f532c6086bc66c29e1bc6c29593e1ca7c',
    ),
    'symfony-cmf/routing' => 
    array (
      'pretty_version' => '2.3.3',
      'version' => '2.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '3c97e7b7709b313cecfb76d691ad4cc22acbf3f5',
    ),
    'symfony/cache' => 
    array (
      'pretty_version' => 'v5.2.6',
      'version' => '5.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '093d69bb10c959553c8beb828b8d4ea250a247dd',
    ),
    'symfony/cache-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c0446463729b89dd4fa62e9aeecc80287323615d',
    ),
    'symfony/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '98606c6fa1a8f55ff964ccdd704275bf5b9f71b3',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '20f73dd143a5815d475e0838ff867bce1eebd9d5',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c87adf3fc1cd0bf4758316a3a150d50a8f957ef4',
    ),
    'symfony/dependency-injection' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '4c41ad68924fd8f9e55e1cd77fd6bc28daa3fe89',
    ),
    'symfony/doctrine-bridge' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e643bddb38277b4a1c2973d1489768c6e6c0db80',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '363cca01cabf98e4f1c447b14d0a68617f003613',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '4204f13d2d0b7ad09454f221bb2195fccdf1fe98',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v1.1.9',
      'version' => '1.1.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '84e23fdcd2517bf37aecbd16967e83f0caee25a7',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1',
      ),
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '940826c465be2690c9fae91b2793481e5cbd6834',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v5.2.4',
      'version' => '5.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '0d639a0943822626290d169965804f79400e6a04',
    ),
    'symfony/framework-bundle' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b5aefc0542e2b42f6f3b9b90d6ef2ff75fee19a',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'v2.3.1',
      'version' => '2.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41db680a15018f9c1d4b23516059633ce280ca33',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '827a00811ef699e809a201ceafac0b2b246bf38a',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '109b2a46e470a487ec8b0ffea4b0bb993aaf42ed',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v5.1.8',
      'version' => '5.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f5485a92c24d4bcfc2f3fc648744fb398482ff1b',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f4ba089a5b6366e453971d3aad5fe8e897b37f41',
    ),
    'symfony/polyfill-iconv' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c536646fdb4f29104dd26effc2fdcb9a5b085024',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3b75acd829741c768bc8b1f84eb33265e7cc5117',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '727d1096295d807c309fb01a851577302394c897',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '39d483bdf39be819deabf04ec872eb0b2410b531',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc6e6f9b39fe8075b3dabfbaf5b5f645ae1340c9',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a678b42e92f86eca04b7fa4c0f6f19d097fb69e2',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e70aa8b064c5b72d3df2abd5ab1e90464ad009de',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '2f4b049fb80ca5e9874615a2a85dc2a502090f05',
    ),
    'symfony/psr-http-message-bridge' => 
    array (
      'pretty_version' => 'v2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '51a21cb3ba3927d4b4bf8f25cc55763351af5f2e',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '826794f2e9305fe73cba859c60d2a336851bd202',
    ),
    'symfony/serializer' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '2af7e86db04ee65fdf1991b17ee0b3e955c93de9',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd15da7ba4957ffb8f1747218be9e1a121fd298a1',
    ),
    'symfony/service-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '73095716af79f610f3b6338b911357393fdd10ab',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2eaa60b558f26a4b0354e1bbb25636efaaad105',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/validator' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d214a3aaa0753b19f94cf0479d8c315d957a10d',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.1.8',
      'version' => '5.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '4e13f3fcefb1fcaaa5efb5403581406f4e840b9a',
    ),
    'symfony/var-exporter' => 
    array (
      'pretty_version' => 'v5.2.4',
      'version' => '5.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5aed4875ab514c8cb9b6ff4772baa25fa4c10307',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '543cb4dbd45ed803f08a9a65f27fb149b5dd20c2',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v2.14.1',
      'version' => '2.14.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5eb9ac5dfdd20c3f59495c22841adc5da980d312',
    ),
    'typo3/phar-stream-wrapper' => 
    array (
      'pretty_version' => 'v3.1.6',
      'version' => '3.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '60131cb573a1e478cfecd34e4ea38e3b31505f75',
    ),
    'webflo/drupal-finder' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c8e5dbe65caef285fec8057a4c718a0d4138d1ee',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6964c76c7804814a842473e0c8fd15bab0f18e25',
    ),
    'webmozart/path-util' => 
    array (
      'pretty_version' => '2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd939f7edc24c9a1bb9c0dee5cb05d8e859490725',
    ),
    'zendframework/zend-diactoros' => 
    array (
      'replaced' => 
      array (
        0 => '^2.2.1',
      ),
    ),
    'zendframework/zend-escaper' => 
    array (
      'replaced' => 
      array (
        0 => '^2.6.1',
      ),
    ),
    'zendframework/zend-feed' => 
    array (
      'replaced' => 
      array (
        0 => '^2.12.0',
      ),
    ),
    'zendframework/zend-stdlib' => 
    array (
      'replaced' => 
      array (
        0 => '^3.2.1',
      ),
    ),
  ),
);
