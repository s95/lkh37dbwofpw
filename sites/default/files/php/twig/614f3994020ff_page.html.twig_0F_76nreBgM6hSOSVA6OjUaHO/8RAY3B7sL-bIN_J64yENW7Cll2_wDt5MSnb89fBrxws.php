<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sites/all/themes/ccc/templates/layout/page.html.twig */
class __TwigTemplate_d585c230e572bf223a4d8e9a0061ebe825b2adbffed50087f4e3f9416326f41d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 46
        $context["nav_classes"] = ((("navbar navbar-expand-lg" . (((        // line 47
($context["b4_navbar_schema"] ?? null) != "none")) ? ((" navbar-" . $this->sandbox->ensureToStringAllowed(($context["b4_navbar_schema"] ?? null), 47, $this->source))) : (" "))) . (((        // line 48
($context["b4_navbar_schema"] ?? null) != "none")) ? ((((($context["b4_navbar_schema"] ?? null) == "dark")) ? (" text-light") : (" text-dark"))) : (" "))) . (((        // line 49
($context["b4_navbar_bg_schema"] ?? null) != "none")) ? ((" bg-" . $this->sandbox->ensureToStringAllowed(($context["b4_navbar_bg_schema"] ?? null), 49, $this->source))) : (" ")));
        // line 51
        echo "
";
        // line 53
        $context["footer_classes"] = (((" " . (((        // line 54
($context["b4_footer_schema"] ?? null) != "none")) ? ((" footer-" . $this->sandbox->ensureToStringAllowed(($context["b4_footer_schema"] ?? null), 54, $this->source))) : (" "))) . (((        // line 55
($context["b4_footer_schema"] ?? null) != "none")) ? ((((($context["b4_footer_schema"] ?? null) == "dark")) ? (" text-light") : (" text-dark"))) : (" "))) . (((        // line 56
($context["b4_footer_bg_schema"] ?? null) != "none")) ? ((" bg-" . $this->sandbox->ensureToStringAllowed(($context["b4_footer_bg_schema"] ?? null), 56, $this->source))) : (" ")));
        // line 58
        echo "\t
<header>
  ";
        // line 60
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header", [], "any", false, false, true, 60), 60, $this->source), "html", null, true);
        echo "  

  <nav class=\"";
        // line 62
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["nav_classes"] ?? null), 62, $this->source), "html", null, true);
        echo "\">
\t 
   <div class=\"";
        // line 64
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["b4_top_container"] ?? null), 64, $this->source), "html", null, true);
        echo " row mx-auto\">
      <div class=\"col-auto p-0\">
      ";
        // line 66
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "nav_branding", [], "any", false, false, true, 66), 66, $this->source), "html", null, true);
        echo "  
      </div>

      <div class=\"col-3 col-md-auto p-0 text-right\">
          <button class=\"navbar-toggler collapsed\" type=\"button\" data-toggle=\"collapse\"
                data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\"
                aria-expanded=\"false\" aria-label=\"Toggle navigation\">
          <span class=\"navbar-toggler-icon\"></span>
        </button>
      </div>

      <div class=\"collapse navbar-collapse col-12 col-md-auto p-0 justify-content-end\" id=\"navbarSupportedContent\">
        ";
        // line 78
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "nav_main", [], "any", false, false, true, 78), 78, $this->source), "html", null, true);
        echo "      
        ";
        // line 79
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "nav_additional", [], "any", false, false, true, 79), 79, $this->source), "html", null, true);
        echo "      
      </div>
    </div>
  </nav>

   <div style=\"clear:both; display:block;\"></div>

</header>

<main role=\"main\">
  <a id=\"main-content\" tabindex=\"-1\"></a>";
        // line 90
        echo "
   <div style=\"clear:both; display:block;\"></div>

    ";
        // line 93
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "contentout1", [], "any", false, false, true, 93)) {
            // line 94
            echo "    <div class=\"container-fluid m-0 p-0 containeroutc2\">
    <div class=\"row containerout1\">
    \t<div class=\"contentout1 col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">
          \t\t";
            // line 97
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "contentout1", [], "any", false, false, true, 97), 97, $this->source), "html", null, true);
            echo "
    \t</div>
       \t<div class=\"contentout2 col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">
    \t\t";
            // line 100
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "contentout2", [], "any", false, false, true, 100), 100, $this->source), "html", null, true);
            echo "
    \t</div>
    </div>
    </div>
    ";
        }
        // line 104
        echo " 

   <div style=\"clear:both; display:block;\"></div>

  ";
        // line 109
        $context["sidebar_first_classes"] = (((twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 109) && twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 109))) ? ("col-12 col-sm-6 col-lg-3") : ("col-12 col-lg-3"));
        // line 111
        echo "  
  ";
        // line 113
        $context["sidebar_second_classes"] = (((twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 113) && twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 113))) ? ("col-12 col-sm-6 col-lg-3") : ("col-12 col-lg-3"));
        // line 115
        echo "  
  ";
        // line 117
        $context["content_classes"] = (((twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 117) && twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 117))) ? ("col-12 col-lg-6") : ((((twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 117) || twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 117))) ? ("col-12 col-lg-9") : ("col-12"))));
        // line 119
        echo "

  <div class=\"";
        // line 121
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["b4_top_container"] ?? null), 121, $this->source), "html", null, true);
        echo "\">
    ";
        // line 122
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "breadcrumb", [], "any", false, false, true, 122)) {
            // line 123
            echo "      ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "breadcrumb", [], "any", false, false, true, 123), 123, $this->source), "html", null, true);
            echo "
    ";
        }
        // line 125
        echo "    
    <div class=\"row no-gutters\">
      ";
        // line 127
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 127)) {
            // line 128
            echo "        <div class=\"order-2 order-lg-1 ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebar_first_classes"] ?? null), 128, $this->source), "html", null, true);
            echo "\">
          ";
            // line 129
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 129), 129, $this->source), "html", null, true);
            echo "
        </div>
      ";
        }
        // line 132
        echo "      
      <div class=\"order-1 order-lg-2 ";
        // line 133
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content_classes"] ?? null), 133, $this->source), "html", null, true);
        echo "\">
        ";
        // line 134
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 134), 134, $this->source), "html", null, true);
        echo "
         
        ";
        // line 136
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "dashboard1", [], "any", false, false, true, 136)) {
            // line 137
            echo "        <div class=\"container-fluid m-0 p-0 dasgboard1c\">
        <div class=\"row dashboard1row\">
        \t<div class=\"dashboard1div col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">
              \t\t";
            // line 140
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "dashboard1", [], "any", false, false, true, 140), 140, $this->source), "html", null, true);
            echo "
        \t</div>
        </div>
        </div>
        ";
        }
        // line 144
        echo " 
        
    
        <div style=\"clear:both; display:block;\"></div>

        

        ";
        // line 151
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "dashboard21", [], "any", false, false, true, 151)) {
            // line 152
            echo "        <div class=\"container-fluid m-0 p-0 dasgboard2c\">
        <div class=\"row dashboard2row\">
        \t<div class=\"dashboard21div col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">
              \t\t";
            // line 155
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "dashboard21", [], "any", false, false, true, 155), 155, $this->source), "html", null, true);
            echo "
        \t</div>
        
        \t<div class=\"dashboard22div col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">
              \t\t";
            // line 159
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "dashboard22", [], "any", false, false, true, 159), 159, $this->source), "html", null, true);
            echo "
        \t</div>
        \t
        \t<div class=\"dashboard23div col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">
              \t\t";
            // line 163
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "dashboard23", [], "any", false, false, true, 163), 163, $this->source), "html", null, true);
            echo "
        \t</div>
        \t\t\t
        </div>
        </div>
        ";
        }
        // line 168
        echo " 

\t

        <div style=\"clear:both; display:block;\"></div>



        ";
        // line 176
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "dashboard31", [], "any", false, false, true, 176)) {
            // line 177
            echo "        <div class=\"container-fluid m-0 p-0 dasgboard3c\">
        <div class=\"row dashboard3row\">
        \t<div class=\"dashboard31div col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">
              \t\t";
            // line 180
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "dashboard31", [], "any", false, false, true, 180), 180, $this->source), "html", null, true);
            echo "
        \t</div>
        
        \t<div class=\"dashboard32div col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">
              \t\t";
            // line 184
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "dashboard32", [], "any", false, false, true, 184), 184, $this->source), "html", null, true);
            echo "
        \t</div>
        </div>
        </div>
        ";
        }
        // line 188
        echo " 
\t

        <div style=\"clear:both; display:block;\"></div>



        
        <div class=\"row container1\">
        \t<div class=\"content1 col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">
              \t\t";
        // line 198
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content1", [], "any", false, false, true, 198), 198, $this->source), "html", null, true);
        echo "
        \t</div>
           \t<div class=\"content2 col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">
        \t\t";
        // line 201
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content2", [], "any", false, false, true, 201), 201, $this->source), "html", null, true);
        echo "
        \t</div>
        </div>
      

        <div class=\"row container2\">
        \t<div class=\"content3 col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">
              \t\t";
        // line 208
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content3", [], "any", false, false, true, 208), 208, $this->source), "html", null, true);
        echo "
        \t</div>
           \t<div class=\"content4 col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">
        \t\t";
        // line 211
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content3", [], "any", false, false, true, 211), 211, $this->source), "html", null, true);
        echo "
        \t</div>
        </div>

    </div>     
     
    ";
        // line 217
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 217)) {
            // line 218
            echo "    <div class=\"order-3 ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebar_second_classes"] ?? null), 218, $this->source), "html", null, true);
            echo "\">
        ";
            // line 219
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 219), 219, $this->source), "html", null, true);
            echo "
    </div>
    ";
        }
        // line 222
        echo "

  </div>


   <div style=\"clear:both; display:block;\"></div>


  </div>


</main>



<div style=\"clear:both; display:block;\"></div>


<div class=\"container-fluid m-0 p-0 ccontainerfull1\">
   <div class=\"row containerfull1\">
\t<div class=\"contentoutfull1 col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">
      \t\t";
        // line 243
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "contentfull1", [], "any", false, false, true, 243), 243, $this->source), "html", null, true);
        echo "
\t</div>
   </div>
</div>

<div style=\"clear:both; display:block;\"></div>

<div class=\"container-fluid m-0 p-0 ccontainerfull2\">
   <div class=\"row containerfull2\">
\t<div class=\"contentoutfull2 col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">
      \t\t";
        // line 253
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "contentfull2", [], "any", false, false, true, 253), 253, $this->source), "html", null, true);
        echo "
\t</div>
   </div>
</div>




</div>



";
        // line 265
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 265)) {
            // line 266
            echo "<footer class=\"mt-auto ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_classes"] ?? null), 266, $this->source), "html", null, true);
            echo "\">
  <div class=\"";
            // line 267
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["b4_top_container"] ?? null), 267, $this->source), "html", null, true);
            echo "\">
    ";
            // line 268
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 268), 268, $this->source), "html", null, true);
            echo "
  </div>
</footer>
";
        }
    }

    public function getTemplateName()
    {
        return "sites/all/themes/ccc/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  396 => 268,  392 => 267,  387 => 266,  385 => 265,  370 => 253,  357 => 243,  334 => 222,  328 => 219,  323 => 218,  321 => 217,  312 => 211,  306 => 208,  296 => 201,  290 => 198,  278 => 188,  270 => 184,  263 => 180,  258 => 177,  256 => 176,  246 => 168,  237 => 163,  230 => 159,  223 => 155,  218 => 152,  216 => 151,  207 => 144,  199 => 140,  194 => 137,  192 => 136,  187 => 134,  183 => 133,  180 => 132,  174 => 129,  169 => 128,  167 => 127,  163 => 125,  157 => 123,  155 => 122,  151 => 121,  147 => 119,  145 => 117,  142 => 115,  140 => 113,  137 => 111,  135 => 109,  129 => 104,  121 => 100,  115 => 97,  110 => 94,  108 => 93,  103 => 90,  90 => 79,  86 => 78,  71 => 66,  66 => 64,  61 => 62,  56 => 60,  52 => 58,  50 => 56,  49 => 55,  48 => 54,  47 => 53,  44 => 51,  42 => 49,  41 => 48,  40 => 47,  39 => 46,);
    }

    public function getSourceContext()
    {
        return new Source("", "sites/all/themes/ccc/templates/layout/page.html.twig", "/home/wmclusterctg/public_html/sites/all/themes/ccc/templates/layout/page.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 46, "if" => 93);
        static $filters = array("escape" => 60);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
