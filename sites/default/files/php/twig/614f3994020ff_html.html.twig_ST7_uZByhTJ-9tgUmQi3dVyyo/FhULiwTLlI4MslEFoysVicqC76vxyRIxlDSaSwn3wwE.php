<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sites/all/themes/ccc/templates/layout/html.html.twig */
class __TwigTemplate_c183ccc6fb33aa88b2e4093a53fe93ed150269af8c49c8d217afe9ac14aa5915 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        $context["body_classes"] = [0 => ((        // line 31
($context["logged_in"] ?? null)) ? ("user-logged-in") : ("")), 1 => (( !        // line 32
($context["root_path"] ?? null)) ? ("path-frontpage") : (("path-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["root_path"] ?? null), 32, $this->source))))), 2 => ((        // line 33
($context["node_type"] ?? null)) ? (("page-node-type-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["node_type"] ?? null), 33, $this->source)))) : ("")), 3 => ((        // line 34
($context["db_offline"] ?? null)) ? ("db-offline") : ("")), 4 => (((        // line 35
($context["b4_body_schema"] ?? null) == "light")) ? (" text-dark") : ((((($context["b4_body_schema"] ?? null) == "dark")) ? (" text-light") : (" ")))), 5 => (((        // line 36
($context["b4_body_bg_schema"] ?? null) != "none")) ? ((" bg-" . $this->sandbox->ensureToStringAllowed(($context["b4_body_bg_schema"] ?? null), 36, $this->source))) : (" ")), 6 => ((        // line 37
($context["logged_in"] ?? null)) ? ("user-logged-innnnn") : ("")), 7 => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 38
($context["app"] ?? null), "request", [], "any", false, false, true, 38), "query", [], "any", false, false, true, 38), "get", [0 => "name"], "method", false, false, true, 38)) ? ("aaaaa") : ("")), 8 => (( !        // line 39
($context["logged_in"] ?? null)) ? ("user-logged-offfff") : ("")), 9 => "d-flex flex-column h-100"];
        // line 43
        echo "
<!DOCTYPE html>
<html";
        // line 45
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["html_attributes"] ?? null), "addClass", [0 => "h-100"], "method", false, false, true, 45), 45, $this->source), "html", null, true);
        echo ">
  <head>
    <head-placeholder token=\"";
        // line 47
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null), 47, $this->source), "html", null, true);
        echo "\">
    <css-placeholder token=\"";
        // line 48
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null), 48, $this->source), "html", null, true);
        echo "\">
    <js-placeholder token=\"";
        // line 49
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null), 49, $this->source), "html", null, true);
        echo "\">

\t<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\">
\t<link href=\"https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap\" rel=\"stylesheet\">
\t<link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css\">

  </head>
  <body";
        // line 56
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["body_classes"] ?? null)], "method", false, false, true, 56), 56, $this->source), "html", null, true);
        echo ">
    ";
        // line 61
        echo "    <a href=\"#main-content\" class=\"visually-hidden focusable skip-link\">
      ";
        // line 62
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Skip to main content"));
        echo "
    </a>
    ";
        // line 64
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page_top"] ?? null), 64, $this->source), "html", null, true);
        echo "
    ";
        // line 65
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page"] ?? null), 65, $this->source), "html", null, true);
        echo "
    ";
        // line 66
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page_bottom"] ?? null), 66, $this->source), "html", null, true);
        echo "
    <js-bottom-placeholder token=\"";
        // line 67
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null), 67, $this->source), "html", null, true);
        echo "\">
        
    <script src=\"https://app.clustermantenimientoctg.com/sites/all/themes/ccc/js/custom.js?";
        // line 69
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_random($this->env), "html", null, true);
        echo "\"></script>

    <link rel=\"stylesheet\" href=\"/sites/all/themes/ccc/css/estilo1.css?";
        // line 71
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_random($this->env), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"/sites/all/themes/ccc/css/estilo2.css?";
        // line 72
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_random($this->env), "html", null, true);
        echo "\">    
    <link rel=\"stylesheet\" href=\"/sites/all/themes/ccc/css/estilo3.css?";
        // line 73
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_random($this->env), "html", null, true);
        echo "\">
 
  </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "sites/all/themes/ccc/templates/layout/html.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 73,  115 => 72,  111 => 71,  106 => 69,  101 => 67,  97 => 66,  93 => 65,  89 => 64,  84 => 62,  81 => 61,  77 => 56,  67 => 49,  63 => 48,  59 => 47,  54 => 45,  50 => 43,  48 => 39,  47 => 38,  46 => 37,  45 => 36,  44 => 35,  43 => 34,  42 => 33,  41 => 32,  40 => 31,  39 => 30,);
    }

    public function getSourceContext()
    {
        return new Source("", "sites/all/themes/ccc/templates/layout/html.html.twig", "/home/wmclusterctg/public_html/sites/all/themes/ccc/templates/layout/html.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 30);
        static $filters = array("clean_class" => 32, "escape" => 45, "t" => 62);
        static $functions = array("random" => 69);

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['clean_class', 'escape', 't'],
                ['random']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
