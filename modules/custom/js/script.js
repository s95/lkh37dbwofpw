(function($) {
  $.fn.scritp = function(data2, data_chart, data_table, procesos) {
	console.log(data_chart)
	console.log(data2)
        console.log(data_table)
        console.log(procesos)
	google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);
     
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Periodo', data_chart[0][2], data_chart[1][2], data_chart[2][2], data_chart[3][2], data_chart[4][2], data_chart[5][2]],
        ['Q1', data_chart[0][3], data_chart[1][3], data_chart[2][3], data_chart[3][3], data_chart[4][3], data_chart[5][3]],
        ['Q2', data_chart[0][4], data_chart[1][4], data_chart[2][4], data_chart[3][4], data_chart[4][4], data_chart[5][4]],
        ['Q3', data_chart[0][5], data_chart[1][5], data_chart[2][5], data_chart[3][5], data_chart[4][5], data_chart[5][5]],
      ]);

      var options = {
        chart: {
          title: 'Brecha por Empresa',
          subtitle: 'Cantidad de empresas con brechas, agrupadas por especialidad: 2021',
        }
      };

      var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

      chart.draw(data, google.charts.Bar.convertOptions(options));

    } 

      if (document.getElementById("text")){document.getElementById("text").innerHTML = "";}
     d = '';
     for (var i = 0; i < data_table.length; i++) {
 	d+= '<tr>'+
    	'<td>'+data_table[i][1]+'</td>'+
	 '<td>'+data_table[i][2]+'</td>'+
 	'<td>'+ 'No cumple '+ data_table[i][2] + ' Requisitos en ' + data_table[i][3]+ ' procesos' +'</td>'+
 	'</tr>';
     }
     $("#customers tr td").remove();
    $("#customers").append(d); 


};
})(jQuery);
