<?php
namespace Drupal\brechas\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Datetime\DrupalDateTime;
use \Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Ajax\HtmlCommand;


/**
 * Provides the form for filter Brecha.
 */
class brechaLicitacionForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'brechas_form';
  }

  /**
   * {@inheritdoc}
   */
 public function buildForm(array $form, FormStateInterface $form_state)
  {


    $procesos = [1 => 'X', 6 => 'A', 12 => 'B'];
    $form['proceso'] = [
      '#type' => 'select',
      '#title' => $this->t('Proceso'),
      '#options' => $procesos,
      '#empty_option' => $this->t('- Select proceso -'),];

     $form['Buscar'] = [
        '#type' => 'button',
      '#value' => $this->t('Buscar'),
        '#ajax' => [
        'callback' => '::logSomething',
      ],];

      $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_message"></div>'
      ];


    $form['#attached']['library'][] = "brechas/data-charts-script";

    return $form;
  }





   public function logSomething(array $form, FormStateInterface $form_state) {

   if(!$form_state->getValue('proceso')){
     $response = new AjaxResponse();
    $response->addCommand(
      new HtmlCommand(
        '.result_message',
        '<p class="my_top_message" id= "text">' . t('La opcion Proceso es  @result', ['@result' => ('REQUERIDA')]) . '</p>'
      )
    );
    return $response; }



  }

/**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form['#attached']['drupalSettings']['data']['year'] = $form_state->getValue('year');
    return $form;
    $year = $form_state->getValue('year');
    $month = $form_state->getValue('month');
    $day = $form_state->getValue('day');
    $messenger = \Drupal::messenger();
    $messenger->addMessage("Year:" . $year  . " Month:" . $month . " Day:" . $day);
 }


}

