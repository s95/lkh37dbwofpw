<?php
namespace Drupal\brechas\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Datetime\DrupalDateTime;
use \Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Ajax\HtmlCommand;


/**
 * Provides the form for filter Brecha.
 */
class brechaForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'brechas_form';	
  }

  /**
   * {@inheritdoc}
   */
 public function buildForm(array $form, FormStateInterface $form_state)
  {

     $query2 = \Drupal::database()->query(" SELECT users_field_data.uid AS uid , users_field_data.name
         FROM
          users_field_data WHERE (users_field_data.status = '1')
         ");
     $data = $query2->fetchAllKeyed();
     $empresa = $form_state->getValue('empresa');

    $form['empresa'] = [
      '#type' => 'select',
      '#title' => $this->t('Empresa'),
      '#options' => $data,
      '#empty_option' => $this->t('- Select Empresa -'),
    ];

    $query = \Drupal::database()->query( " SELECT taxonomy_term_field_data.tid AS tid, taxonomy_term_field_data.name  FROM
    {taxonomy_term_field_data} taxonomy_term_field_data
    WHERE (taxonomy_term_field_data.status = '1') AND (taxonomy_term_field_data.vid IN ('especialidad'))
    LIMIT 100 OFFSET 0");
    $especialidades = $query->fetchAllKeyed();    

    $form['especialidad'] = [
      '#type' => 'select',
      '#title' => $this->t('Especialidad'),
      '#options' => $especialidades,
      '#empty_option' => $this->t('- Select especialidad -'),
    ];

    $periodos = [1 => 'Ultimos 30 dias', 6 => 'Ultimo semestre', 12 => 'ultimo Año'];  	
    $form['periodo'] = [
      '#type' => 'select',
      '#title' => $this->t('Periodo'),
      '#options' => $periodos,
      '#empty_option' => $this->t('- Select periodo -'),];
   
     $form['Buscar'] = [
        '#type' => 'button',
      '#value' => $this->t('Buscar'),
	'#ajax' => [
        'callback' => '::logSomething',
      ],];
    
      $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_message"></div>'
      ];


    $form['#attached']['library'][] = "brechas/data-charts-script";

    return $form;
  }





   public function logSomething(array $form, FormStateInterface $form_state) {

    $query = "SELECT nodefield_loglicitacion.field_loglicitacion_target_id AS nodefieldloglicitacion_field_loglicitacion_target_id, nodefield_logproveedor.field_logproveedor_target_id AS nodefield_logproveedor_field_logproveedor_target_id,
    nodefield_logrequisito.field_logrequisito_target_id AS node_field_logrequisito_field_logrequisito_target_id, node_field_data.created,  node_field_data.nid AS nid, node_field_data.type, userdata.name, userdata.uid,
    cumple.field_logrequisitocumple_value, especialidad.field_especialidad_target_id

    FROM
    node_field_data

    LEFT JOIN node__field_loglicitacion nodefield_loglicitacion ON node_field_data.nid = nodefield_loglicitacion.entity_id AND nodefield_loglicitacion.deleted = '0' AND (nodefield_loglicitacion.langcode = node_field_data.langcode
     OR nodefield_loglicitacion.bundle = 'logproceso')

    LEFT JOIN node__field_logproveedor nodefield_logproveedor ON node_field_data.nid = nodefield_logproveedor.entity_id AND nodefield_logproveedor.deleted = '0'

    LEFT JOIN node__field_logrequisito nodefield_logrequisito ON node_field_data.nid = nodefield_logrequisito.entity_id AND nodefield_logrequisito.deleted = '0'
    INNER JOIN users_field_data userdata ON nodefield_logproveedor.field_logproveedor_target_id = userdata.uid
    INNER JOIN taxonomy_term__field_especialidad especialidad ON nodefield_logrequisito.field_logrequisito_target_id = especialidad.entity_id
    INNER JOIN node__field_logrequisitocumple cumple ON node_field_data.nid = cumple.entity_id";

   
   if(!$form_state->getValue('periodo') || !$form_state->getValue('empresa')){
     $response = new AjaxResponse();
    $response->addCommand(
      new HtmlCommand(
        '.result_message',
        '<p class="my_top_message" id= "text">' . t('La opcion Periodo y empresa son  @result', ['@result' => ('REQUERIDAS')]) . '</p>'
      )
    );
    return $response;           };


   if($form_state->getValue('empresa') &&  !$form_state->getValue('especialidad') && $form_state->getValue('periodo')){
    $where = " WHERE  (node_field_data.type IN ('logcumplimiento')) AND (node_field_data.created >= unix_timestamp( current_date - interval {$form_state->getValue('periodo')} month )) AND (userdata.uid = {$form_state->getValue('empresa')}) ";
    $query = $query . $where;
    $query = \Drupal::database()->query($query);};


   if($form_state->getValue('empresa') && $form_state->getValue('especialidad') && $form_state->getValue('periodo')){
    $where = " WHERE (node_field_data.status = '1') AND (node_field_data.type IN ('logcumplimiento')) AND (node_field_data.created >= unix_timestamp( current_date - interval {$form_state->getValue('periodo')} month )) AND (especialidad.field_especialidad_target_id = {$form_state->getValue('especialidad')})
     AND (userdata.uid = {$form_state->getValue('empresa')})";
    $query = $query . $where; 
    $query = \Drupal::database()->query($query);};

    $data2 = $query->fetchAll(); 
    $procesos = [];
    $procesosnum = 0;
    $data_chart = [[0,'14','IC - Instrumentación y control',0,0,0],[1,'15','CV - Civil',0,0,0],[2,'16','EE - Eléctrica y electrónica',0,0,0],[3,'45','MC - Mecánica',0,0,0],[4,'46','MT - Metalmecánica',0,0,0],[5,'47','SR - Servicios y Productos Transversales',0,0,0]];
    $data_table = [['14','IC - Instrumentación y control',0,$procesos],['15','CV - Civil',0,$procesosnum],['16','EE - Eléctrica y electrónica',0,$procesosnum],['45','MC - Mecánica',0,$procesosnum], ['46','MT - Metalmecánica',0,$procesosnum],['47','SR - Servicios y Productos Transversales',0,$procesosnum]];
    $Q1= array(1,2,3,4);
    $Q2= array(5,6,7,8);
    $Q3= array(9,10,11,12);
    foreach ($data2 as $objt) {
      if($objt->field_logrequisitocumple_value === "0"){
        if(!in_array($objt->nodefieldloglicitacion_field_loglicitacion_target_id, $procesos)){
          array_push($procesos,$objt->nodefieldloglicitacion_field_loglicitacion_target_id);
          $procesosnum = sizeof($procesos);
          $data_table[0][3] =  $procesosnum;
          $data_table[1][3] =  $procesosnum;
          $data_table[2][3] =  $procesosnum;
          $data_table[3][3] =  $procesosnum;
          $data_table[4][3] =  $procesosnum;
          $data_table[5][3] =  $procesosnum;
         };
        $especialiad_id = 0;
        foreach ($data_chart as $especialidad){
	  if($objt->field_especialidad_target_id === $especialidad[1]){
	   $especialidad_id = $especialidad[0];
	   $data_table[$especialidad_id][2] = $data_table[$especialidad_id][2] + 1;
           $date = DrupalDateTime::createFromTimestamp( $objt->created );
	   if(in_array($date->format('n'), $Q1)){
             $data_chart[$especialidad_id][3] = $data_chart[$especialidad_id][3] + 1;
             break;
          };
          if(in_array($date->format('n'), $Q2)){
            $data_chart[$especialidad_id][4] = $data_chart[$especialidad_id][4] + 1;
             break;
           };
          if(in_array($date->format('n'), $Q3)){
             $data_chart[$especialidad_id][5] = $data_chart[$especialidad_id][5] + 1;
             break;
             };

         };
        };
      };
    };
    $keys = array_column($data_table, 2);
    array_multisort($keys, SORT_DESC, $data_table);
    $response = new AjaxResponse();
    $response->addCommand(
      new InvokeCommand(NULL, 'scritp', [$data2, $data_chart,$data_table,$procesos,])
    );
    return $response;
  }

/**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form['#attached']['drupalSettings']['data']['year'] = $form_state->getValue('year');
    return $form;
    $year = $form_state->getValue('year');
    $month = $form_state->getValue('month');
    $day = $form_state->getValue('day');
    $messenger = \Drupal::messenger();
    $messenger->addMessage("Year:" . $year  . " Month:" . $month . " Day:" . $day);
 }


}
	
