<?php

namespace Drupal\brechas\Controller;

use Drupal\Core\Controller\ControllerBase;


class Brecha extends ControllerBase{
  public function view()
    {$fname = \Drupal::request()->query->get('fname');
	$marks = \Drupal::request()->query->get('marks');
	
	//====load filter controller
	$form['form'] = $this->formBuilder()->getForm('Drupal\brechas\Form\brechaForm');

    $query = \Drupal::database()->query( " SELECT taxonomy_term_field_data.tid AS tid, taxonomy_term_field_data.name  FROM
{taxonomy_term_field_data} taxonomy_term_field_data
WHERE (taxonomy_term_field_data.status = '1') AND (taxonomy_term_field_data.vid IN ('especialidad'))
LIMIT 100 OFFSET 0  " );

    $query2 = \Drupal::database()->query("SELECT users_field_data.uid AS uid, users_field_data.name 
FROM
{users_field_data} users_field_data ");

     $resul = $query->fetchAll();
     $resul2 = $query2->fetchAll();

      return [
      '#theme' => 'template',
      '#result' => $resul,
      '#result2' => $resul2,
      '#form' => $form
    ];
    }

}
