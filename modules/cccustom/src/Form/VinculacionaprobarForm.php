<?php
  
namespace Drupal\ex81\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
  
/**vvv
 * HelloForm controller.
 */
class VinculacionaprobarForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'ex81_vinculacionaprobar_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
   
  public function buildForm(array $form, FormStateInterface $form_state) {

    // start Messanger class; 
    $messenger = \Drupal::messenger();


    //get the gets
    $g_uid          = (array_key_exists('uid', $_GET) ? $_GET['uid'] : ''); 
    $g_redirect     = (array_key_exists('redirect', $_GET) ? $_GET['redirect'] : '');     
    $g_tiporegistro = (array_key_exists('tipo', $_GET) ? $_GET['tipo'] : '');     


    // validate parameters
    if ( strlen($g_uid) == 0)
    {
        $messenger->addMessage('Error: Debe enviar el parámetro de usuario asociado a esta acción.','error');
        return $form;
    }

    if (!is_numeric($g_uid)) {
        $messenger->addMessage('Error: Parámetro de usuario inválido.','error');
        return $form;
    } 
    
    if ( strlen($g_redirect) == 0)
    {
        $messenger->addMessage('Error: Debe enviar el parámetro de redirección asociado a esta acción.','error');
        return $form;
    } 

    if ( strlen($g_tiporegistro) == 0)
    {
        $messenger->addMessage('Error: Debe enviar el parámetro de tipo de registro asociado a esta acción.','error');
        return $form;
    }

    if ( $g_tiporegistro != 'comprador' && $g_tiporegistro != 'proveedor' )
    {
        $messenger->addMessage('Error: Debe enviar un parámetro de tipo de registro válido.','error');
        return $form;
    }


    if ($user = User::load($g_uid))
    {

      $form['intro'] = [
        '#markup' => '<p>' . $this->t('Confirme que desea aprobar la vinculación de esta empresa:') . '</p>',
      ];
   
      $form['user'] = [
        '#theme' => 'item_list',
        '#items' => [],
      ];


      $form['user']['#items'][] = [
        '#markup' => $this->t('Nombre:') . ' ' . $user->field_empresanombre->value,
      ];

      $form['user']['#items'][] = [
        '#markup' => $this->t('Nombre:') . ' ' . $user->field_nit->value,
      ];


      // $form['user']['#items'][] = [
      //   '#markup' => $this->t('Username:') . ' ' . $user->getDisplayName(),
      // ];

      $form['user']['#items'][] = [
        '#markup' => $this->t('Email:') . ' ' . $user->getEmail(),
      ];


// (( $r_period = $user->field_r_period_length_sec->value;


      $form['actions'] = [
        '#type' => 'container',
      ];


      $form['actions']['approve'] = [
        '#type' => 'submit',
        '#value' => $this->t('Confirmar'),
        '#attributes' => [
          'class' => [
            'button',
            'button--primary',
          ],
        ],
      ];


      $form['actions']['cancelar'] = [
        '#type' => 'submit',
        '#value' => $this->t('Cancelar'),
        '#attributes' => [
          'class' => [
            'button',
            'button--secondary',
          ], 
        ],
      ];


      $form['uid'] = [
        '#type' => 'hidden',
        '#value' => $g_uid,
      ];


      $form['tiporegistro'] = [
        '#type' => 'hidden',
        '#value' => $g_tiporegistro,
      ];


    }



//    $form['accept'] = [
//      '#type' => 'checkbox',
 //     '#title' => $this
 //       ->t('I accept the terms of use of the site'),
  //    '#description' => $this->t('Please read and accept the terms of use'),
//    ];


    // Add a submit button that handles the submission of the form.
    // $form['actions']['submit'] = [
    //   '#type' => 'submit',
    //   '#value' => $this->t('Submit'),
    // ];

    return $form;

  }

  /**
   * Validate the title and the checkbox of the form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // $title = $form_state->getValue('title');
    // $accept = $form_state->getValue('accept');

    // if (strlen($title) < 10) {
    //   // Set an error for the form element with a key of "title".
    //   $form_state->setErrorByName('title', $this->t('The title must be at least 10 characters long.'));
    // }

    // if (empty($accept)) {
    //   // Set an error for the form element with a key of "accept".
    //   $form_state->setErrorByName('accept', $this->t('You must accept the terms of use to continue'));
    // }

  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();

    $messenger->addMessage('Aqui llegué.');
        
  }  

        
/*    // Activate the user. 
    $user = User::load($form_state->getValue('uid'));
    $user->activate();
    $user->save();

    // Let the activator know the user has been activated.
    $message = $this->t('The user has been successfully activated.');
    \Drupal::messenger()->addMessage($message, 'status');

    // Redirect back to admin people.
    $response = new RedirectResponse('/admin/people');
    $response->send();
  }
  */
  
/*   public function submitForm(array &$form, FormStateInterface $form_state) {

    // Display the results
    // Call the Static Service Container wrapper
    // We should inject the messenger service, but its beyond the scope
    // of this example.
    $messenger = \Drupal::messenger();
    $messenger->addMessage('Title: ' . $form_state->getValue('title'));
    $messenger->addMessage('Accept: ' . $form_state->getValue('accept'));

    // Redirect to home.
    $form_state->setRedirect('<front>');
  }
*/ 

}
