<?php

namespace Drupal\ex81\Form;
use Drupal\Core\Entity; 
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url; 
use Drupal\user\Entity\User;


/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class ConfirmDeleteForm extends ConfirmFormBase {

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;

  /**
   * {@inheritdoc}
   */

  /**
   * form id
   */
    public function getFormId() : string {
        return "confirm_vinculacionaprobar";
    }

    public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL) {

        // start Messanger class; 
        $messenger = \Drupal::messenger();
        
        // validar parameter id
        if (!is_numeric($id)) {
            $messenger->addMessage('Error: Parámetro de usuario inválido.','error');
            return $form;
        }  


        // load user          
        if ($user = User::load($id))
        {

            $form['intro'] = [
                '#markup' => '<p>' . $this->t('Confirme que desea aprobar la vinculación de esta empresa:') . '</p>',
            ];
            
            $form['user'] = [
                '#theme' => 'item_list',
                '#items' => [],
            ];
            
            
            $form['user']['#items'][] = [
                '#markup' => $this->t('#:') . ' ' . $id,
            ];
            
            $form['user']['#items'][] = [
                '#markup' => $this->t('Nombre:') . ' ' . $user->field_empresanombre->value,
            ];
            
            $form['user']['#items'][] = [
                '#markup' => $this->t('NIT/TIN/RUC/CNPJ/RUT/CUIT:') . ' ' . $user->field_nit->value,
            ];
        
            $form['user']['#items'][] = [
            '#markup' => $this->t('Email:') . ' ' . $user->getEmail(),
            ];
    
        } 
        else 
        {
            $messenger->addMessage('Error: Entidad no existe.','error');

            return $form;
        }



      $form['uid'] = [
        '#type' => 'hidden',
        '#value' => $id,
      ];


   //   $form['tiporegistro'] = [
   //     '#type' => 'hidden',
    //    '#value' => $g_tiporegistro,
    //  ];

        
    return parent::buildForm($form, $form_state);
  }
   
  /**
   * {@inheritdoc} 
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
        $uid = $form_state->getValue('uid'); 

        // start Messanger class; 
        $messenger = \Drupal::messenger();

        // save data          
        if ($user = User::load($uid))
        {
            $user->addRole('proveedor');
            $user->removeRole('preregistro');            
            $user->activate();      
            $user->save();


            $messenger->addMessage('El proveedor ha sido aprobado.');

        }
        else 
        {
            $messenger->addMessage('Error: Entidad no existe.','error');
        }

        // Redirect to home.
        $url = Url::fromUserInput('/ccc/vinculacion-solicitudes');
        $form_state->setRedirectUrl($url);     

  } 

    
      
  /**
   * redirect to cancel URL 
  */
  public function getCancelUrl() { 
        $path = 'internal:/ccc/vinculacion-solicitudes';
        $url = Url::fromUri($path);
        return $url;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Desea aprobar como proveedor esta empresa # %id?', ['%id' => $this->id]); 
  }

}


